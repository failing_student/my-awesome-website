# About
I've started this project in order to learn html, css and also how to explain things.  It mostly consists of information on C++ and C as these are the only 2 things I feel I know enough about.  The articles are written either in Romanian or English but I intend to make them available in both languages.  Though I am trying my hardest I am still a newbie in computer science and programming so take my articles with a grain of salt. The intended audience is made of people who know nothing of or are beginners to C and C++.

# Hosting
This website is not currently hosted anywhere and I don't have any short term goal of getting it online.

# Contribute
As this is a project I've made for self-improvement, code contributing might not be the best idea, nevertheless if you would still like to, I will try to merge it into the project.
The best way I feel anyone could contribute is by a donation (I know, how greedy), luckily for one's pocket, I am not used to being paid for things like this so even a minuscule amount should be sufficient to create the incentive to work more.

# Requests
If would like me to cover a specific topic, feel free to open an issue and I'll see what I can do.
